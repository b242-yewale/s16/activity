console.log("Hello Onkar!")

// JavaScript Operators

// Arithmetic Operatots

let x = 1397;
let y = 7831;

// Sum
let sum = x + y;
console.log("Result of addition operator: "+sum);

// Difference
let difference = x - y;
console.log("Result of substraction operator: "+difference);

// Product
let product = x * y;
console.log("Result of multiplication operator: "+product);

// Quotient
let quotient = x / y;
console.log("Result of division operator: "+quotient);

// Modulus : returns remainder of division
let remainder = x % y;
console.log("Result of modulus operator: "+remainder);

// Assignment Operators
// Basic assignment operator (=)
let assignmentNumber = 8;

// Addition assignment operator (+=)
assignmentNumber += 2;
console.log("Result of addition assignment operator: "+assignmentNumber)

// Substraction assignment operator (-=)
assignmentNumber -= 2;
console.log("Result of substraction assignment operator: "+assignmentNumber)

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: "+assignmentNumber)

assignmentNumber /= 2;
console.log("Result of division assignment operator: "+assignmentNumber)

// Multiple Operators and Parenthesis
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: "+mdas);

// The order of operations can be changed by adding parentheses to the logic
let bodmas = 1 + (2 - 3) * (4 / 5);
console.log("Result of bodmas operation: "+bodmas)

// Increment and Decrement
let z = 1;

// Pre-increment
let increment = ++z;
console.log("Result of pre-increment: "+increment);
console.log("Result of pre-increment: "+ z);

// Post-increment
increment = z++;
console.log("Result of post-increment: "+increment);
console.log("Result of post-increment: "+ z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: "+decrement);
console.log("Result of pre-decrement: "+ z);

// Post-decrement
decrement = z--;
console.log("Result of post-decrement: "+decrement);
console.log("Result of post-decrement: "+ z);
